package cat.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ImageView imageViewTitle;
    ImageButton resultTextView;
    ImageButton resultTextView2;
    Button rollButton;
    Button buttonReset;
    int[] textureArrayWin = {
            R.drawable.dice_1,
            R.drawable.dice_2,
            R.drawable.dice_3,
            R.drawable.dice_4,
            R.drawable.dice_5,
            R.drawable.dice_6,
            R.drawable.empty_dice,
    };

    private int num1, num2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewTitle = findViewById(R.id.imageViewTitle);
        resultTextView = findViewById(R.id.result_textview);
        resultTextView2 = findViewById(R.id.result_textview2);
        rollButton = findViewById(R.id.roll_button);
        buttonReset = findViewById(R.id.buttonReset);

        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num1 = (int) (Math.random() * 6);
                num2 = (int) (Math.random() * 6);
                if (num1 == 5 && num2 == 5){
                    Toast mensaje = Toast.makeText(getApplicationContext(), "JACKPOT!", Toast.LENGTH_SHORT);
                    mensaje.setGravity(Gravity.TOP|Gravity.LEFT, 0, 0);
                    mensaje.show();
                }
                resultTextView.setImageResource(textureArrayWin[num1]);
                resultTextView2.setImageResource(textureArrayWin[num2]);
                //rollButton.setText("Dice Rolled");
                //Toast.makeText(getApplicationContext(), "He apretat el botó", Toast.LENGTH_SHORT).show();
            }
        });

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultTextView.setImageResource(textureArrayWin[6]);
                resultTextView2.setImageResource(textureArrayWin[6]);
            }
        });

        resultTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1 = (int) (Math.random() * 6);
                resultTextView.setImageResource(textureArrayWin[num1]);
            }
        });

        resultTextView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num2 = (int) (Math.random() * 6);
                resultTextView2.setImageResource(textureArrayWin[num2]);
            }
        });
    }
}